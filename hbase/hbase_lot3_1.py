#hbase shell'''''' #scan 'fromagerie',  {FILTER => "SingleColumnValueFilter('cf', 'datcde', =, 'substring:2020') AND SingleColumnValueFilter('cf', 'villecli', =, 'substring:NANTES')"

import csv
import pandas as pd
from connection import connection_hbase

'''
    Programme qui détermine la meilleure commande de Nantes de l’année 2020 et l’enregistre dans un fichier csv.
'''

# Connexion à HBase
connection = connection_hbase()
connection.open()

# Nom de la table HBase
ma_table_name = 'fromagerie'

# Accédez à la table
table = connection.table(ma_table_name)

# Année spécifique
annee_recherchee = '2020'
ville_recherchee = 'NANTES'

# Filtrer les résultats pour l'année 2020 et la ville NANTES
filter_str = f"SingleColumnValueFilter('cf', 'datcde', =, 'substring:{annee_recherchee}') AND SingleColumnValueFilter('cf', 'villecli', =, 'substring:{ville_recherchee}')"
rows = table.scan(filter=filter_str)

# Créer une liste de dictionnaires pour stocker les données
data_list = []

# Ajouter les résultats du scan à la liste
for key, data in rows:
    data_dict = {'Row': key.decode('utf-8')}
    for column, value in data.items():
        data_dict[column.decode('utf-8')] = value.decode('utf-8')
    data_list.append(data_dict)

# Créer un DataFrame à partir de la liste de dictionnaires
df = pd.DataFrame(data_list)

# Trier le DataFrame par la colonne 'cf:qte' de manière décroissante
df_sorted = df.sort_values(by='cf:qte', ascending=False)

# Afficher le DataFrame trié
##print("\nDataFrame trié par 'cf:qte':")
##print(df_sorted)

# Prendre la première valeur de la colonne 'cf:qte' (la plus grande)
max_qte = df_sorted['cf:qte'].iloc[0]

# Afficher le maximum de 'cf:qte'
##print("\nMaximum de la colonne 'cf:qte':", max_qte)

# Trouver l'indice de la première occurrence du maximum dans la colonne 'cf:qte'
idxmax_qte = df['cf:qte'].idxmax()

# Afficher la ligne correspondant au maximum de 'cf:qte'
row_max_qte = df.loc[idxmax_qte]

# Afficher la ligne
print("\nLa meilleur commande de Nantes de l’année 2020 est : ")
print(row_max_qte)

# Enlever les guillemets doubles autour des valeurs dans la ligne
row_max_qte = row_max_qte.apply(lambda x: x.replace('"', ''))

# Enregistrer la ligne dans un fichier CSV avec des virgules comme séparateurs
csv_file = "../resultats/lot3_1_meilleur_commande_Nantes_2020.csv"

with open(csv_file, 'w', newline='', encoding='utf-8') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',')
    csv_writer.writerow(row_max_qte.values[1:])

# Fermer la connexion à HBase
connection.close()
