import pandas as pd
from connection import connection_hbase

'''
    Programme qui détermine le nom, le prénom, le nombre de commande et la somme des quantités d’objets 
    du client qui a eu le plus de frais de timbrecde et et l’enregistre dans un fichier excel.
'''

# Connexion à HBase
connection = connection_hbase()
connection.open()

# Sélection de la table HBase
table_name = 'fromagerie'
table = connection.table(table_name)

# Définition de la famille de colonnes
column_family = b'cf'

# Requête pour récupérer les clients avec les frais de timbrecde
scan_result = table.scan(columns=[b'cf:nomcli', b'cf:prenomcli', b'cf:codcli', b'cf:timbrecde', b'cf:qte', b'cf:codcde'])

data_list = []

# Ajouter les résultats du scan à la liste
for key, data in scan_result:
    data_dict = {'Row': key.decode('utf-8')}
    for column, value in data.items():
        data_dict[column.decode('utf-8')] = value.decode('utf-8')
    data_list.append(data_dict)

# Créer un DataFrame à partir de la liste de dictionnaires
df = pd.DataFrame(data_list)
#print(df)

# Supprimer la première ligne qui contient les libellés des colonnes
df = df.iloc[1:]

# Supprimer les guillemets doubles et les espaces autour des valeurs
df['cf:nomcli'] = df['cf:nomcli'].str.strip('"')
df['cf:prenomcli'] = df['cf:prenomcli'].str.strip('"')
df['cf:qte'] = df['cf:qte'].str.strip('"')
df['cf:timbrecde'] = df['cf:timbrecde'].str.strip('"')
df['cf:codcde'] = df['cf:codcde'].str.strip('"')


# Convertir les colonnes 'cf:qte' et 'cf:timbrecde' en types numériques
df['cf:qte'] = pd.to_numeric(df['cf:qte'], errors='coerce')
df['cf:timbrecde'] = pd.to_numeric(df['cf:timbrecde'], errors='coerce')
df['cf:codcde'] = pd.to_numeric(df['cf:codcde'], errors='coerce')

# Afficher les premières lignes pour vérifier les modifications
df_new = df.groupby(['cf:nomcli','cf:prenomcli']).agg({'cf:codcde' : 'nunique', 'cf:qte' : 'sum','cf:timbrecde' : 'sum'}).reset_index()


df_sorted = df_new.sort_values(by='cf:timbrecde', ascending=False)

df = df_sorted.iloc[[0]]

df.to_excel('../resultats/lot3_3_client_avec_max_frais_timbrecde.xlsx', index=False)

print(df)
connection.close()

