import happybase

'''
    Methode qui permet de se connecter à la base Hbase.
'''

def connection_hbase():
    '''
    hostname et port à modifier en fonction de votre config
    '''

    # Mbaye
    hostname = 'node175996-env-1839015-etudiant24.sh1.hidora.com'
    port = 11651

    # Chantal
    hostname= 'node175997-env-1839015-etudiant25.sh1.hidora.com'
    port=11664

    '''
    On se connecte de notre Host H1 pour interroger notre client Hbase
    de notre Docker sur le port 16010 : ZooKeeper : HMASTER
    '''

    connection=happybase.Connection(
        host=hostname,
        port=port
    )
    return connection
