import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from connection import connection_hbase

ma_connection = connection_hbase()
ma_connection.open()

'''
    Programme qui détermine le nombre total de commandes effectuées entre 2010 et 2015, réparties par année 
    et exporte le graphe barplot matplotib en pdf.
'''

table_name = 'fromagerie'
table = ma_connection.table(table_name)

print("Entre 2010 et 2015")
list_rows = []

for annee in ['2010', '2011', '2012', '2013', '2014', '2015']:
    filter_str = f"SingleColumnValueFilter('cf', 'datcde', =, 'substring:{annee}')"
    rows = table.scan(columns=[b'cf:datcde', b'cf:codcde'], filter=filter_str)
    list_rows.append(rows)

# concatenation de plusieurs villes dans un seule dataframe
data_list = []
for rows_years in list_rows:
    for key, data in rows_years:
        data_dict = {'Row': key.decode('utf-8')}
        for column, value in data.items():
            data_dict[column.decode('utf-8')] = value.decode('utf-8')
        data_list.append(data_dict)

df = pd.DataFrame(data_list)

df['cf:datcde'] = df['cf:datcde'].str.strip('"')
df['cf:datcde'] = pd.to_datetime(df['cf:datcde'], format='%Y-%m-%d %H:%M:%S')
nouveau_df = df.copy()
nouveau_df['Annee'] = nouveau_df['cf:datcde'].dt.year
resultats_par_annee = nouveau_df.groupby('Annee')['cf:codcde'].count().reset_index()
print(resultats_par_annee)

# barplot matplotib exporté en pdf
pdf_file = "../resultats/lot3_2_cmd_par_annee.pdf"
with PdfPages(pdf_file) as export_pdf:
    plt.bar(resultats_par_annee['Annee'], resultats_par_annee['cf:codcde'])
    plt.title('Total de commandes effectuées entre 2010 et 2015 \nréparties par année\n')
    plt.xlabel('Annee')
    plt.ylabel('Nombre de commandes')
    export_pdf.savefig()
    plt.close()

# Fermer la connexion à HBase
ma_connection.close()
