
from hbase.connection import connection_hbase

connection = connection_hbase()
connection.open()

#mettre le nom de la table
table_name = 'xxxx'

table = connection.table(table_name)

try:
    connection.disable_table(table_name)
    connection.delete_table(table_name)
    print(table_name, "supprimée")
except:
    print(table_name, "n'existe pas")

try:
    connection.create_table(table_name, {'cf': dict()})
    print(table_name, "créé")
except:
    print(table_name, "existe déjà")
