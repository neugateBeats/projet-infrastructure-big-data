import csv

from hbase.connection import connection_hbase

'''
    Programme d'import du fichier csv dans Hbase
'''

ma_connection = connection_hbase()
ma_connection.open()

table_name = 'fromagerie'

table = ma_connection.table(table_name)

with open('../resources/dataw_fro03.csv', newline='', encoding='utf-8') as csvfile:
    filereader = csv.reader(csvfile, delimiter=',', quotechar='|')
    index = 0
    for row in filereader:

        codcli, genrecli, nomcli , prenomcli, cpcli, villecli, codcde, datcde, timbrecli, timbrecde = (
         row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9])

        Nbcolis, cheqcli, barchive,	bstock,	codobj,	qte, Colis,	libobj,	Tailleobj, Poidsobj = (
         row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19])

        points, indispobj, libcondit, prixcond,	puobj = (row[20], row[21], row[22], row[23], row[24])

        # verifie qu'il n'y ait pas de vide ou "NULL"
        cpcli = cpcli if cpcli and (cpcli != "NULL") else "0"
        genrecli = genrecli if genrecli and (genrecli != "NULL") else ""
        nomcli = nomcli if nomcli and (nomcli != "NULL") else ""
        prenomcli = prenomcli if prenomcli and (prenomcli != "NULL") else ""
        cpcli = cpcli if cpcli and (cpcli != "NULL") else "0"
        villecli = villecli if villecli and (villecli != "NULL") else ""
        codcde = codcde if codcde and (codcde != "NULL") else "0"
        datcde = datcde if datcde and (datcde != "NULL") else ""
        timbrecli = timbrecli if timbrecli and (timbrecli != "NULL") else "0"
        timbrecde = timbrecde if timbrecde and (timbrecde != "NULL") else "0"
        Nbcolis = Nbcolis if Nbcolis and (Nbcolis != "NULL") else "0"
        cheqcli = cheqcli if cheqcli and (cheqcli != "NULL") else "0"
        barchive = barchive if barchive and (barchive != "NULL") else "0"
        bstock = bstock if bstock and (bstock != "NULL") else "0"
        qte = qte if qte and (qte != "NULL") else "0"
        Colis = Colis if Colis and (Colis != "NULL") else "0"
        libobj = libobj if libobj and (libobj != "NULL") else ""
        Tailleobj = Tailleobj if Tailleobj and (Tailleobj != "NULL") else ""
        Poidsobj = Poidsobj if Poidsobj and (Poidsobj != "NULL") else "0"
        points = points if points and (points != "NULL") else "0"
        indispobj = indispobj if indispobj and (indispobj != "NULL") else "0"
        libcondit = libcondit if libcondit and (libcondit != "NULL") else ""
        prixcond = prixcond if prixcond and (prixcond != "NULL") else "0"
        puobj = puobj if puobj and (puobj != "NULL") else "0"

        table.put(b'%i' % index,
                  {b'cf:codcli': '%s' % codcli
                      , b'cf:genrecli': '%s' % genrecli
                      , b'cf:nomcli': '%s' % nomcli
                      , b'cf:prenomcli': '%s' % prenomcli
                      , b'cf:cpcli': '%s' % cpcli
                      , b'cf:villecli': '%s' % villecli
                      , b'cf:codcde': '%s' % codcde
                      , b'cf:datcde': '%s' % datcde
                      , b'cf:timbrecli': '%s' % timbrecli
                      , b'cf:timbrecde': '%s' % timbrecde
                      , b'cf:Nbcolis': '%s' % Nbcolis
                      , b'cf:cheqcli': '%s' % cheqcli
                      , b'cf:barchive': '%s' % barchive
                      , b'cf:bstock': '%s' % bstock
                      , b'cf:codobj': '%s' % codobj
                      , b'cf:qte': '%s' % qte
                      , b'cf:Colis': '%s' % Colis
                      , b'cf:libobj': '%s' % libobj
                      , b'cf:Tailleobj': '%s' % Tailleobj
                      , b'cf:Poidsobj': '%s' % Poidsobj
                      , b'cf:points': '%s' % points
                      , b'cf:indispobj': '%s' % indispobj
                      , b'cf:libcondit': '%s' % libcondit
                      , b'cf:prixcond': '%s' % prixcond
                      , b'cf:puobj': '%s' % puobj
                   })
        index += 1

print(table)

ma_connection.close()
