<<<<<<< HEAD
# projet-infrastructure-big-data

Ce projet a été réalisé par:
- BOUKANZIA Chaimae
- ELIE DIT COSAQUE Patrice
- GUEYE Mbaye
- GUILLAUME Chantal

Le projet se décompose en plusieurs parties réparties en lots :
- lot 1 et lot 2 : 
Création de jobs pour limiter le flux d’information (Mapper-Reducer) pour obtenir uniquement les informations voulues pour répondre au besoin du client.
- lot 3 : 
Mise en place une base NoSQL HBASE pour stocker le contenu du fichier CSV afin d’interroger ce Data Warehouse avec des scripts python.
Création d’un programme python avec Panda pour créer des graphes en pdf et des tableaux Excel et csv de notre importation dans HBase.
- lot 4 :
Mise en œuvre un moteur de recherche avec Power BI pour interroger le Data Warehouse HBase.

## Structure du projet :
![](documentation/repertoires_projet.jpg)
### documentation 
Répertoire qui contient la documentation du projet
### map_reduce 
Répertoire qui contient les programmes Python du lot 1 et 2. 
- mapper_lot1.py : programme qui permet de filtrer les données selon les critères suivants : entre 2006 et 2010, avec uniquement les départements 53, 61 et 28
- mapper_lot1.py : programme qui permet de filtrer les données selon les critères suivants : entre 2011 et 2016, avec uniquement les départements 22, 49 et 53 et Le timbrecli non renseigné ou à 0
- reducer_lot1.py : programme qui permet de faire ressortir dans un tableau des 100 meilleures commandes avec la ville, la somme des quantités des articles et la valeur de « timbrecde » (la notion de meilleures commandes : la somme des quantités la plus grande ainsi que le plus grand nombre de « timbrecde » ). Exporter le résultat dans un fichier Excel.
- reducer_lot2.py : programme qui permet de faire ressortir de façon aléatoire de 5% des 100 meilleures commandes avec la ville, la somme des quantités des articles sans « timbrecli » avec la moyenne des quantités de chaque commande. Avoir un PDF avec un graphe (PIE) par Ville. Exporter le résultat dans un fichier Excel.

Les mapper reducer seront lancés depuis la VM à l’aide du script  vm_total_lot.sh. Usage :  vm_total_lot.sh lot1  ou  vm_total_lot.sh lot2

Il faudra mettre au préalable dans un dossier ‘projet’ situé à la racine de la vm :
- les mapper et reducer
- le fichier csv 
- les scripts vm_before_lot.sh, vm_after_lot.sh, vm_total_lot.sh, hadoop_lot.sh 

Il faudra aussi donner les droits d'exécution aux scripts par la commande 
	chmod 755 *.sh

### scripts  
Répertoire qui contient les scripts permettant de lancer les mapper et reducer 
- vm_before_lot.sh : permet de démarrer les dockers, copier les fichiers, lancer les slaves
- hadoop_lot.sh : pour démarrer hadoop, mettre le fichier csv dans input, run mapper et Reducer, afficher le contenu de /datavolume1
- vm_after_lot.sh : pour copier les fichiers lotx de /datavolume1 dans projet sur la VM
- vm_total_lot.sh : pour lancer l'execution du mapper reducer du lot souhaité. Ce script appelle les 3 scripts précédents.

Préalablement à l'exécution des programmes de hbase et à l’utilisation du fromagerie.pbix dans power BI il faut démarrer l'environnement HBase :
- Dans  la VM
  * ./start_docker_digi.sh : lancement de l'environnement Docker.
  * ./lance_srv_slaves.sh : lancement des serveurs sur les nœuds esclaves d’un cluster.
  * ./bash_hadoop_master.sh : ouvrir un shell Bash sur le noeud maître de votre cluster Hadoop.
    
- Dans hadoop-master
  * ./start-hadoop.sh : démarrer les services Hadoop sur le nœud maître et les nœuds esclaves du cluster.
  * stop-hbase.sh : arrêter les services HBase sur les nœuds du cluster.
  * ./services_hbase_thrift.sh: lancement des services Thrift de Hbase qui permet de se connecter et interagir avec Hbase.
  * start-hbase.sh : démarre les services Hbase sur les nœuds du cluster.
  * Hbase shell : ouvrir l’interface en ligne de commande Hbase pour exécuter des commandes Hbases directement depuis le terminal.


### hbase : 
Répertoire qui contient les programmes Python du lot 3. 
- connection.py : Methode qui permet de se connecter à la base Hbase.
- import_csv_to_hbase.py : programme d'import du fichier csv dans Hbase
- hbase_lot3_1.py : programme qui détermine la meilleure commande de Nantes de l’année 2020 et l’enregistre dans un fichier csv.
- hbase_lot3_2.py : programme qui détermine le nombre total de commandes effectuées entre 2010 et 2015, réparties par année et exporte le graphe barplot matplotib en pdf.
- hbase_lot3_3.py : programme qui détermine le nom, le prénom, le nombre de commande et la somme des quantités d’objets du client qui a eu le plus de frais de timbrecde et et l’enregistre dans un fichier excel.

Les différents programmes seront lancés dans un interpréteur style Pycharm (l’environnement hbase doit au préalable être démarré).

### powerBI 
Répertoire qui contient le fichier power BI fromagerie.pbix du lot 4

Préalablement à l’utilisation du fromagerie.pbix dans Power BI il faut démarrer l'environnement HBase  et avoir configurer la connection à la base de données :

Pour avoir la connection à la base de données
  - ./hbase_odbc_rest.sh: lancement des services ODBC et REST de Hbases afin de se connecter, d’interagir et récupérer la table dans Hbase sur Power BI.

 - Sur le PC où Power BI est installé:
    Il faut configurer la source de données ODBC:
    * Connexion Bureau à distance sur machine virtuelle (si besoin)
    * Ouvrir l’application ODBC
    * Configurez les paramètres de connexion en mettant le nom de notre serveur étudiant (Hbase), le port et nom utilisateur.
    * Testez la connexion afin de vérifier qu’elle fonctionne correctement.

Lorsque l'environnement Hbase est opérationnel:
    * Ouvrir Power BI Desktop
    * Ouvrir le fichier fromagerie.pbix

### resources:
Répertoire qui contient le fichier csv fourni par le client.
Le fichier dataw_fro03.csv est constituée de 25 colonnes et de plus de 135000 lignes.
Ce fichier stocke les données issues du datawarehouse de la Fromagerie.

### traitement_BD:
Répertoire qui contient le programme detection_accents.py
Ce script permet de détecter les accents des données dans chaque colonne de la table. 
Une fois que le script est lancé, il affiche les colonnes contenant des accents, et à la main, on fait les modifications sur le fichier dataw_fro03.csv.

### resultats:
Répertoire qui contient les résultats des différents lots
- lot1.xlsx : résultat des scripts mapper_lot1 et reducer_lot1, 
- lot2.xlsx et lot2_graphique_par_ville.pdf: résultat des scripts mapper_lot2 et reducer_lot2
- lot3_1_meilleur_commande_Nantes_2020.csv : résultat du script Hbase_lot3_1
- lot3_2_cmd_par_annee.pdf : résultat du script Hbase_lot3_2 
- lot3_3_client_avec_max_frais_timbrecde.xlsx : résultat du script Hbase_lot3_3

## Configurations:
- Hadoop avec Python 3.5.2 et Hbase 
- Python avec Pycharm 3.11: 
  * Librairie pandas
  * Librairie Matplotlib
  * Librairie Csv
  * Librairie Happybase
- Power BI version 2.124.1805.0 64-bit

## Environnement de travail: 
- Machine virtuelle: 
  * Windows Server 2022 Standard
  * Processor : Intel Xeon Processor (Cascadelake)   2.49 GHz
  * Installed Ram : 32,0 GB
- Environnement Docker:
  * 1 Hadoop master
  * 2 Hadoop slaves


=======
# Projet Infrastructure Big Data



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/neugateBeats/projet-infrastructure-big-data.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/neugateBeats/projet-infrastructure-big-data/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> 3396145fdb1bd20dde069d08ff579fc1ac948c8b
