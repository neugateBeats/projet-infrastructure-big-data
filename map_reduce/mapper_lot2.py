#!/usr/bin/env python
import sys
import csv
from datetime import datetime

'''
    Programme qui permet de filtrer les données selon les critères suivants :
    Entre 2011 et 2016,
    Avec uniquement les départements 22, 49 et 53
    Le timbrecli non renseigné ou à 0
'''

# Créez un lecteur CSV pour gérer les données
csv_reader = csv.reader(sys.stdin)

# Ignorer la première ligne (en-tête)
next(csv_reader, None)

# Parcourez les lignes du CSV
for row in csv_reader:
    # Extraire les champs du CSV
    cpcli, villecli, codcde, datcde, timbrecli, qte = (
        row[4],
        row[5],
        row[6],
        row[7],
        row[8],
        row[15],
    )

    '''
    print des données entre 2011 et 2016 pour les départements 22, 49 et 53
    '''
    if datcde != "NULL":
        # si datcde n'est pas à NULL on récupère l'année
        datcde_annee = datetime.strptime(datcde, '%Y-%m-%d %H:%M:%S').year

        if (datcde_annee >= 2011 and datcde_annee <= 2016):
            # données entre 2011 et 2016
            code_dep = cpcli[0:-3]  # supprime les 3 derniers caractères du cpcli
            if (int(code_dep) == 22 or int(code_dep) == 49 or int(code_dep) == 53):
                # avec uniquement les départements 22, 49 et 53
                if timbrecli == "NULL" or timbrecli == '0' :
                    # le timbrecli est non renseigné ou à 0
                    print("%s\t%s\t%s" % (codcde, villecli, qte))
