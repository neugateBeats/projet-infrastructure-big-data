#!/usr/bin/env python
"""reducer_lot1.py"""
import os
import sys
import decimal
import pandas as pd
import openpyxl
'''
    Programme qui permet de faire ressortir dans un tableau des 100 meilleures commandes 
    avec la ville, la somme des quantités des articles et la valeur de « timbrecde » (la notion
    de meilleures commandes : la somme des quantités la plus grande ainsi que le plus
    grand nombre de « timbrecde » ) 
    Exporter le résultat dans un fichier Excel
'''

# Fonction pour traiter une ligne de données et mettre à jour le dictionnaire mydata
def process_line(line, commandes):

    # remove leading and trailing whitespace
    line = line.strip()
    # print(line)
    # splitting the data on the basis of tab we have provided in mapper.py
    item = line.split('\t')
    # get codcde, cpcli, villecli, datcde, timbrecde, qte from result of Mapper
    codcde = int(item[0])
    villecli = item[1]
    timbrecde = float(item[2])
    qte = int(item[3])

    if codcde in commandes.keys():
        # la codcde est déjà dans commandes, on met à jour la valeur de la liste contenant villecli, qte, timbrecde
        points_commande = commandes.get(codcde)
        points_commande[0] = villecli
        points_commande[1] += qte
        points_commande[2] += timbrecde
        commandes[codcde] = points_commande

    else:
        # la codcde n'exite pas dans commandes, on ajoute le codcde avec comme valeur une liste contenant villecli, qte, timbrecde
        points_commande = [villecli, qte, timbrecde]
        commandes[codcde] = points_commande

# Dictionnaire
commandes = {}

# Lecture de l'entrée standard
for line in sys.stdin:
    process_line(line, commandes)

# Triez le dictionnaire en fonction de la clé "points_commande"
sorted_commandes = sorted(
    commandes.items(), key=lambda x: x[1][1], reverse=True
)

print(sorted_commandes)

# Créez un DataFrame à partir des données
mydata = []

for codcde, points_commande in sorted_commandes[:100]:
    # Données à ajouter dans le tableau mydata
    mydata.append([codcde, points_commande[0], points_commande[1] , points_commande[2] ])

# Donner à mettre dans le dataframe : mydata
df = pd.DataFrame(mydata, columns=['code_commande','ville', 'quantité', 'timbre_commande' ])

# Tri par quantité puis timbre_commande
df = df.sort_values(by = ['quantité', 'timbre_commande'], ascending = [False, False])

# Enregistrez le DataFrame dans un fichier Excel
excel_file = "/datavolume1/lot1.xlsx"
df.to_excel(excel_file, index=False)

