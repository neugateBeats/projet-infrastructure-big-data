#!/usr/bin/env python
"""reducer_lot2.py"""

import sys
import pandas as pd
import random
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

'''
    Programme qui permet de faire ressortir de façon aléatoire de 5% des 100 meilleures
    commandes avec la ville, la somme des quantités des articles sans « timbrecli » 
    avec la moyenne des quantités de chaque commande
    Avoir un PDF avec un graphe (PIE) par Ville 
    Exporter le résultat dans un fichier Excel.
'''

# Fonction pour traiter une ligne de données et mettre à jour le dictionnaire commandes
def process_line(line, commandes):
    # remove leading and trailing whitespace
    line = line.strip()
    # print(line)
    # splitting the data on the basis of tab we have provided in mapper.py
    item = line.split('\t')
    # get codcde, villecli,qte from result of Mapper
    codcde = int(item[0])
    villecli = item[1]
    qte = int(item[2])

    if codcde in commandes.keys():
        # la codcde est déjà dans commandes, on met à jour la valeur de la liste contenant villecli, qte
        points_commande = commandes.get(codcde)
        points_commande[0] = villecli
        points_commande[1] += qte
        points_commande[2] += 1
        commandes[codcde] = points_commande

    else:
        # la codcde n'exite pas dans commandes, on ajoute le codcde avec comme valeur une liste contenant villecli, qte
        points_commande = [villecli, qte, 1]
        commandes[codcde] = points_commande


# Dictionnaire
commandes = {}

# Lecture de l'entrée standard
for line in sys.stdin:
    process_line(line, commandes)

# Triez le dictionnaire en fonction de la clé "points_commande"
sorted_commandes = sorted(
    commandes.items(), key=lambda x: x[1][1], reverse=True
)
print(sorted_commandes)

# Sélectionnez 5% des 100 meilleures commandes de façon aléatoire
selected_commandes = random.sample(sorted_commandes[:100], int(0.05 * 100))

# Créez un DataFrame à partir des données sélectionnées
mydata = []
for codcde, points_commande in selected_commandes:
    # Données à ajouter dans le tableau mydata
    ville = points_commande[0]
    sum_qte = points_commande[1]
    avg_qte = sum_qte / points_commande[2]

    mydata.append([codcde, ville, sum_qte, avg_qte])

# Données à mettre dans le dataframe : mydata
df = pd.DataFrame(mydata, columns=["code_commande", "ville", "sum_quantite", "avg_quantite"])

# Enregistrez le DataFrame dans un fichier Excel
excel_file = "/datavolume1/lot2.xlsx"
df.to_excel(excel_file, index=False)


# Dictionnaire pour stocker les données par ville
donnees_ville = {}

# Collecte des données par ville
for codcde, points_commande in selected_commandes:
    ville = points_commande[0]
    sum_qte = points_commande[1]

    if ville in donnees_ville:
        donnees_ville[ville] += sum_qte
    else:
        donnees_ville[ville] = sum_qte

# Création du graphe PIE
fig, ax = plt.subplots()
ax.pie(donnees_ville.values(), labels=donnees_ville.keys(), autopct='%1.1f%%', startangle=90)
ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

# Enregistrement du graphe dans un fichier PDF
pdf_file = "/datavolume1/lot2_graphique_par_ville.pdf"
with PdfPages(pdf_file) as pdf:
    pdf.savefig(fig)


