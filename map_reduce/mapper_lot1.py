#!/usr/bin/env python
import sys
import csv
from datetime import datetime

'''
    Programme qui permet de filtrer les données selon les critères suivants :
    Entre 2006 et 2010,
    Avec uniquement les départements 53, 61 et 28
'''

# Créez un lecteur CSV pour gérer les données
csv_reader = csv.reader(sys.stdin)

# Ignorer la première ligne (en-tête)
next(csv_reader, None)

# Parcourez les lignes du CSV
for row in csv_reader:
    # Extraire les champs du CSV
    cpcli, villecli, codcde, datcde, timbrecde, qte = (
        row[4],
        row[5],
        row[6],
        row[7],
        row[9],
        row[15],
    )

    '''
    print des données entre 2006 et 2010 pour les départements 28, 53 et 61
    '''
    if datcde != "NULL":
        # si datcde n'est pas à NULL on récupère l'année
        datcde_annee = datetime.strptime(datcde, '%Y-%m-%d %H:%M:%S').year

        if (datcde_annee >= 2006 and datcde_annee <= 2010) :
            # données entre 2006 et 2010
            code_dep = cpcli[0:-3]  # supprime les 3 derniers caractères du cpcli

            if (int(code_dep) == 28 or int(code_dep) == 53 or int(code_dep) == 61):
                # avec uniquement les départements 53, 61 et 28
                print("%s\t%s\t%s\t%s" % (codcde, villecli, timbrecde, qte))
