#!/bin/bash

# test le nombre d'arguments
if [ $# -ne 1 ]; then
 echo "Vous devez passer lot1 ou lot2 en argument "

else
  echo "Execution de vm_before_lot pour $1"

  # met le premier argument dans la variable lot
  cd /root

  # Exécutez le script start_docker_digi.sh pour démarrer les conteneurs Docker
  echo ""
  echo "démarrer les conteneurs Docker"
  ./start_docker_digi.sh

  # donne les droits d'execution
  echo ""
  echo "donne les droits d'execution"
  chmod 755 ./projet/hadoop_lot.sh


  # Copiez les fichiers dans le conteneur hadoop_master
  echo ""
  echo "Copie des fichiers dans le conteneur hadoop_master"
  docker cp ./projet/dataw_fro03.csv hadoop-master:/root/
  docker cp ./projet/mapper_$1.py hadoop-master:/root/
  docker cp ./projet/reducer_$1.py hadoop-master:/root/
  docker cp ./projet/hadoop_lot.sh hadoop-master:/root/

  # Lancez les serveurs esclaves Hadoop
  echo ""
  echo "Lancez les serveurs esclaves Hadoop"
  ./lance_srv_slaves.sh


  # Exécutez le script bash_hadoop_master.sh pour accéder au conteneur hadoop-master
#  echo ""
#  echo "Accéder au conteneur hadoop-master"
#  ./bash_hadoop_master.sh
fi