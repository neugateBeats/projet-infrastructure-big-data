#!/bin/bash

# test le nombre d'arguments
if [ $# -ne 1 ]; then
  echo "Vous devez passer lot1 ou lot2 en argument "

else

  echo "Execution de vm_after_lot pour $1"


  # Copiez les fichiers de /datavolume1 du lot depuis le conteneur hadoop-master dans le répertoire projet
  if [ $1 == "lot1" ]; then
    echo "fichier /datavolume1/lot1.xlsx de hadoop dans vm"
   docker cp hadoop-master:/datavolume1/$1.xlsx .
  else
    echo "fichier /datavolume1/lot2.xlsx de hadoop dans vm"
    docker cp hadoop-master:/datavolume1/$1.xlsx .
    echo "fichier /datavolume1/lot2_graphique_par_ville.pdf de hadoop dans vm"
    docker cp hadoop-master:/datavolume1/$1_graphique_par_ville.pdf .
  fi
fi
