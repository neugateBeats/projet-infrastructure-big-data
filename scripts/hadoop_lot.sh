#!/bin/bash

# test le nombre d'arguments
if [ $# -ne 1 ]; then
 echo "Vous devez passer lot1 ou lot2 en argument "

else

  echo "Execution de hadoop_lot pour $1"

  # Démarrez les services Hadoop
  echo ""
  echo "Démarre les services Hadoop"
  ./start-hadoop.sh

  # supprime le fichier dataw_fro03.csv de input de hadoop
  echo ""
  echo "Supprime le fichier dataw_fro03.csv de input de hadoop"
  hdfs dfs -rm input/dataw_fro03.csv
  # met le fichier dataw_fro03.csv dans input de hadoop
  echo "Met le fichier dataw_fro03.csv dans input de hadoop"
  hdfs dfs -put dataw_fro03.csv input
  echo "Supprime le fichier output$1 de output de hadoop"
  # supprime le fichier outputlotx de output de hadoop
  hdfs dfs -rm -r output/output$1

  # run Mapper and Reducer
  echo ""
  echo "run Mapper and Reducer sur $1"
  hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.7.2.jar -file mapper_$1.py -mapper "python3 mapper_$1.py" -file reducer_$1.py -reducer "python3 reducer_$1.py" -input input/dataw_fro03.csv -output output/output$1

  echo ""
  ls -l /datavolume1
fi