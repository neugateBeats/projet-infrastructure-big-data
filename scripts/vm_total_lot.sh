#!/bin/bash
# script qui permet lancer l'execution du mapper reducer du lot souhaité depuis la VM 

# test le nombre d'arguments
if [ $# -ne 1 ]; then
  echo "Vous devez passer lot1 ou lot2 en argument "

else

  echo "Execution de vm_total_lot pour $1"

  # Dans la VM
  ./vm_before_lot.sh $1

  # Execute ./hadoop_lot.sh dans le container hadoop-master depuis la VM
  docker exec -ti hadoop-master ./hadoop_lot.sh $1

  # Dans la VM
  ./vm_after_lot.sh $1
fi