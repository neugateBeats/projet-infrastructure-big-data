# Importation des bibliothèques nécessaires
import pandas as pd
from unidecode import unidecode

# Lecture du fichier CSV dans un DataFrame
df = pd.read_csv('dataw_fro03.csv')

# Affichage du DataFrame original et des types de données de chaque colonne
print("Dataframe original :")
print(df)
types_colonnes = df.dtypes
print(types_colonnes)

# Sélection des colonnes de type texte
colonnes_texte = df.select_dtypes(include=['object']).columns

# Définition d'une fonction (a_des_accents) pour vérifier si une chaîne de caractères contient des accents
def a_des_accents(chaine):
    if isinstance(chaine, str):
        chaine_sans_accents = unidecode(chaine)
        return chaine != chaine_sans_accents
    return False

# Parcours des colonnes de texte pour détecter celles qui contiennent des accents
colonnes_avec_accents = []
for colonne in colonnes_texte:
    if any(df[colonne].apply(a_des_accents)):
        colonnes_avec_accents.append(colonne)

# Affichage des colonnes contenant des accents, le cas échéant
if colonnes_avec_accents:
    print("Colonnes avec des accents :", colonnes_avec_accents)
else:
    print("Aucune colonne avec des accents.")

# Pour chaque colonne avec des accents, affichage des exemples de lignes contenant des caractères accentués
for colonne in colonnes_avec_accents:
    lignes_avec_accents = df[df[colonne].apply(a_des_accents)]
    if not lignes_avec_accents.empty:
        print(f"\nExemples de lignes dans la colonne '{colonne}' avec des accents :")
        print(lignes_avec_accents.head())
